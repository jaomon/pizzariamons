class CreatePedidos < ActiveRecord::Migration
  def change
    create_table :pedidos do |t|
      t.datetime :data
      t.string :status
      t.float :total

      t.integer :cliente_id
      t.integer :pizza_id

      t.timestamps
    end
  end
end
