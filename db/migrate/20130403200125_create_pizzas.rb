class CreatePizzas < ActiveRecord::Migration
  def change
    create_table :pizzas do |t|
      t.string :sabor
      t.string :valor

      t.timestamps
    end
  end
end
