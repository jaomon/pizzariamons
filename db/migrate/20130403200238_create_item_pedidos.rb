class CreateItemPedidos < ActiveRecord::Migration
  def change
    create_table :item_pedidos do |t|
      t.integer :quantidade
      t.float :valor
      t.string :observacao

      t.timestamps
    end
  end
end
