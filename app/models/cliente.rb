class Cliente < ActiveRecord::Base
	has_many :pedido

	attr_accessible :endereco, :nome, :telefone

	validates :endereco, presence: true
	validates :nome, presence: true
	validates :telefone , presence: true
	
end
