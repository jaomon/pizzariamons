class Pedido < ActiveRecord::Base
  belongs_to :cliente
  has_many :item_pedido

  attr_accessible :data, :status, :total
end
