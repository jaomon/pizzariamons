class ItemPedido < ActiveRecord::Base  
  belongs_to :pedido
  has_many :pizza

  attr_accessible :observacao, :quantidade, :valor

  validates :quantidade, presense: true
end
