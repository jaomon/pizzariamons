class Usuario < ActiveRecord::Base
	attr_accessible :login, :senha

	validates :login, presence: true
	validates :senha, presence: true
	
end
