class ItemPedidosController < ApplicationController
  # GET /item_pedidos
  # GET /item_pedidos.json
  def index
    @item_pedidos = ItemPedido.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @item_pedidos }
    end
  end

  # GET /item_pedidos/1
  # GET /item_pedidos/1.json
  def show
    @item_pedido = ItemPedido.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @item_pedido }
    end
  end

  # GET /item_pedidos/new
  # GET /item_pedidos/new.json
  def new
    @item_pedido = ItemPedido.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @item_pedido }
    end
  end

  # GET /item_pedidos/1/edit
  def edit
    @item_pedido = ItemPedido.find(params[:id])
  end

  # POST /item_pedidos
  # POST /item_pedidos.json
  def create
    @item_pedido = ItemPedido.new(params[:item_pedido])

    respond_to do |format|
      if @item_pedido.save
        format.html { redirect_to @item_pedido, notice: 'Item pedido was successfully created.' }
        format.json { render json: @item_pedido, status: :created, location: @item_pedido }
      else
        format.html { render action: "new" }
        format.json { render json: @item_pedido.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /item_pedidos/1
  # PUT /item_pedidos/1.json
  def update
    @item_pedido = ItemPedido.find(params[:id])

    respond_to do |format|
      if @item_pedido.update_attributes(params[:item_pedido])
        format.html { redirect_to @item_pedido, notice: 'Item pedido was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @item_pedido.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /item_pedidos/1
  # DELETE /item_pedidos/1.json
  def destroy
    @item_pedido = ItemPedido.find(params[:id])
    @item_pedido.destroy

    respond_to do |format|
      format.html { redirect_to item_pedidos_url }
      format.json { head :no_content }
    end
  end
end
