PizzariaMon::Application.routes.draw do
  resources :item_pedidos

  resources :pedidos

  resources :pizzas

  resources :usuarios

  resources :clientes

  get "login/login"

  root  :to => "login#index"

end
